# Install Docker Desktop on Mac OS

Download and install it from <https://hub.docker.com/editions/community/docker-ce-desktop-mac>.

Or install Homebrew <https://brew.sh/> and then in a terminal run

```
brew cask install docker
```

Once installed, find Docker.app in your applications folder and run it. Once the docker icon in your system tray stops animating, open a terminal and try

```
docker --version
```

If this reports the docker version, you are all set.

> ! Note that `cask` is important above. If you leave it out, you'll install the docker client, but not the complete docker system.

---
Copyright 2020, Stoney Jackson <dr.stoney@gmail.com>

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
