# Docker - Advantages and Disadvantages

## Objectives

* Identify the advantages and disadvantages of Docker

## Model 1 - Overview

![Model 1](model.jpg)

- App = Application
- Deps = Application depedencies (e.g., Python, Java, 3rd party libraries, environment variables, files, etc.)
- OS = Operating System
- HW = Hardware

1. Model 1 depicts four methods of deployment of applications. Write a title and a description for each method.

    Method | Title | Description
    ------- | ----- | -----------
    A | App-per-machine | Each app and its dependencies 
    B | |
    C | |
    D | |

2. For each characteristic below, place each method on a scale of *least* to *most*. Be prepared to share your reasoning.

    1. Efficiently uses hardware and operating system resources.
    (This one is completed as an example.)

        ```
        least                        most
        A         C             D      B
        ```
        
        - B makes best use of HW and OS resources as apps and dependencies can share.
        - A makes the least. If apps are small, then HW and OS will be underutilized.
        - C shares the hardware. But each app has its own OS. That's going to take up a lot memory,
          which is a waste if apps use the same OS.
        - D shares hardware and software, but there may be a bit more overhead with docker.


    2. Is difficult to install/deploy due to incompatabilities between requirements of applications and their dependencies.

        ```
        least                        most

        ```

    3. Is fast to deploy an application and all its dependendencies. Include the operating system and hardware as a dependency if it is not shared with other applications.

        ```
        least                        most

        ```

    4. Requires applications and their dependencies to built for the same operating system?

        ```
        least                        most

        ```

    5. Demands a lot of hardware resources from each machine.

        ```
        least                        most

        ```

    6. Allows applications and their dependencies to interfere with each other.

        ```
        least                        most

        ```



---

&copy; 2020 Stoney Jackson <dr.stoney@gmail.com>

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.