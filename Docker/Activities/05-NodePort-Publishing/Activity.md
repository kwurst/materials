## Model 11

Objectives

- Map a port to a container.

Run and observe.

```
docker run --detach nginx
docker ps
```

1. What ports are open on the nginx server?

2. Open a browser to http://localhost:80/ . What do you get?

3. Open a brower to http://127.0.0.1:80/ . What do you get?

4. Stop and remove the container.

Run and observe.

```
docker run --detach --port 8888:80 nginx
```

Open a browser to http://localhost:80/ .

Open a browser to http://localhost:8888/ .

5. What does `--port 8888:80` do?

6. Stop and remove the container.

7. Run an nginx server that receives requests on localhost port 9000. What command did you use?

8. Run another nginx server on port 90001 and test it.

9. Stop and remove all containers.

## Model 12

Objectives

- View the logs of a running container.

Instructions

1. Start an nginx server that listens on port 9000.
2. Identify the name or ID of your container.
2. Run (replace ID_OR_NAME with your container's ...)
    ```
    docker logs ID_OR_NAME
    ```
3. Open a browser to http://localhost:9000/
4. Run
    ```
    docker logs ID_OR_NAME
    ```

## Model 13

Reflect on your how well your team is functioning. 

1. What's working?

2. What can be improved?

3. What changes will you make to improve?

---

&copy; 2020 Stoney Jackson <dr.stoney@gmail.com>

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.