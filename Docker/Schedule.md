
## Text

```
Docker in Action, Second Edition
by Stephen Kuenzli; Jeffrey Nickoloff
Published by Manning Publications, 2019
```

Available on learning.oreilly.com for free via an ACM membership (student $19/yr).

## Schedule

| Reading                  | Activity
| ------------------------ | ---------------------------------- 
|                          | 00-Install
| Chapter 1 - all          | 01-Advantages-Disadvantages
| Chapter 2 - stop at 2.3  | 02-Managing-Images-and-Containers
| Chatper 3 - all          | 03-Semantic-Versioning
| Chapter 4 - stop at 4.3  | 04-Bind-Mounting
| Chapter 5 - 5.1-5.2, 5.4 | 05-NodePort-Publishing
| Chapter 8 - stop at 8.3  | 06-Building-Images

