# Team setup

1. Discord
    - Download, install and log into
    - Get into your group's voice channel
    - If all members are physically present, mute your mics
    - If you at least one remote member, have one mic on in the classroom.
2. Determine team roles (replace NAME with the name of the person who will fill that role)
    Role | Name | Responsibilities
    ----- | ------- | -------------------
    Driver | NAME | Carries out instructions on behalf of the team. Shares screen with team on Discord.
    Recorder | NAME | Reads out the question and records the team's answers.
    Manager* | NAME | Facilitates discussions, ensures everyone has a voice, manages time, ensures the team works as a team. Handles remote communication for remote members.
    * If you have only 2 members, merge recorder and manager.
3. Everyone opens a browser and loads teams eitherpad.